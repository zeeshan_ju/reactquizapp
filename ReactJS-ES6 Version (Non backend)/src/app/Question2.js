import React from 'react';

export class Question2 extends React.Component {
	constructor(props) {
		super();
		this.state = {
			question: "Who created ReactJS?",
			answer: "Facebook"
		}
	}

	onHandleChange(event) {
		if (event.target.value == this.state.answer) 
			this.props.toSelect(true);
		else
		this.props.toSelect(false);
	}

	render () {
		return (
			<div onChange={(event) => this.onHandleChange(event)}>
        <p>{this.state.question}</p>
        	<input type="radio" value="Google"/>
        	Google
        	<br />

        	<input type="radio" value="Facebook"/>
        	Facebook
        	<br />

        	<input type="radio" value="Microsoft"/>
        	Microsoft
          	<br />

        	<input type="radio" value="IBM"/>
        	IBM
        	<br />
      </div>
		)
	}
}
