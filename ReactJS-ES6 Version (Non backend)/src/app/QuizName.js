import React from 'react';

export class QuizName extends React.Component {
	render() {
		return (
			<div>
				<h1> Hey there! Take this quiz on {this.props.name} : </h1>	
				<h4> Scoring: <br /> Correct answer: +2 <br /> Incorrect answer: -1 </h4>
			</div>
		);
	}
}
