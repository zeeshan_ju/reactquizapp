import React from 'react';

export class Question1 extends React.Component {
	constructor(props) {
		super();
		this.state = {
			question: "What is ReactJS?",
			answer: "JavaScript library"
		}
	}

	onHandleChange(event) {
		if (event.target.value == this.state.answer) 
			this.props.toSelect(true);
		else
			this.props.toSelect(false);
	}

	render () {
		return (
			<div onChange={(event) => this.onHandleChange(event)}>
      	<p>{this.state.question}</p>
        	<input type="radio" value="Python library"/>
        	Python library
        	<br />

        	<input type="radio" value="Python framework"/>
        	Python framework
        	<br />

        	<input type="radio" value="JavaScript library"/>
        	JavaScript library
        	<br />

        	<input type="radio" value="JavaScript framework"/>
        	JavaScript framework
        	<br />
      </div>
		)
	}
}
