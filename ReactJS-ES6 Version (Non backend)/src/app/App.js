import React from 'react';
import { render } from 'react-dom';
import {QuizName} from './QuizName.js'
import {Question1} from './Question1.js'
import {Question2} from './Question2.js'

class App extends React.Component {
	constructor () {
		super();
		this.state = {
			score: 0
		};
	}

	onSelect(val) {
		if (val) 
			this.setState ({
				score: this.state.score + 2
			});
		else 
			this.setState ({
				score: this.state.score - 1
			});	
	}

	onSubmit() {
		alert("Your Score: " + this.state.score);
	}

	render() {
		return (
			<div>
				<QuizName name="ReactJS"/>
    		<form id="react-quiz" name="react-quiz">
    			<Question1 toSelect={this.onSelect.bind(this)}/>

    			<Question2 toSelect={this.onSelect.bind(this)}/>

        	<input type="button" value="Submit" 
        		onClick={()=>this.onSubmit()} />
    		</form>
			</div>
		);
	}
}

render (<App />, window.document.getElementById ('app'));